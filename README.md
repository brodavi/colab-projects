colab-projects
==============

A simple API for listing and creating/editing projects

clone the repo
==============
    $ git clone https://github.com/colab-coop/colab-projects.git

install deps (requires npm):
============================
    $ npm install

edit config file and copy
=========================
    $ cp config-dev.js config.js

run service
===========
    $ node app.js

the api
=======

### get all someModels
GET _/projects_

### get project
GET _/project/:projectCode_

### create project
POST _/projects_ // with JSON payload matching model

### update project
PUT _/project/:projectCode_ // with JSON payload matching model