#!/bin/bash

forever start -a \
    -l forever.log \
    -o out.log \
    -e error.log \
    --uid "colab-projects" app.js