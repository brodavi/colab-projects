'use strict';
/* User module is to be done, in the meanwhile here is a small module to allow authorization */

var passport = require('passport');
var users = require('../../config').config.users;
var BasicStrategy = require('passport-http').BasicStrategy;

function findByIdAndPassword(id, password, fn) {
  for (var i = 0, len = users.length; i < len; i++) {
    var user = users[i];
    if (user.username === id && user.password === password) {
      return fn(null, user);
    }
  }
  return fn(null, null);
}

passport.use(new BasicStrategy({
  },
  function(username, password, done) {
    process.nextTick(function () {
      findByIdAndPassword(username, password, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        return done(null, user);
      });
    });
  }
));