var mongoose = require('mongoose');
var Project = process.app.connection.model('Project');
var _ = require('highland');

module.exports = {
  create: function (req, res, next) {
    var project = new Project(req.body);
    project.save(function (err) {
      if (err) return res.jsonp(400, err);
      return res.jsonp(project);
    });
  },

  update: function (req, res, next) {
    var code = req.params.code;
    Project
      .findOne({'code': code})
      .exec(function (err, project) {
        _.extend(req.body, project);
        project.save(function(err){
          if (err) {
            return res.send(err);
          }
          else {
            return res.status(200).json(project);
          }
        });
      });
  },

  all: function (req, res, next) {
    Project
      .find()
      .exec(function (err, projects) {
        if (err) return res.send(400, err);

        return res.json(projects);
      });
  },

  get: function (req, res, next) {
    var code = req.params.code;
    Project
      .findOne({code: code})
      .exec(function (err, project) {
        if (err) return res.send(400, err);
        return res.json(project);
      });
  },

  addRoutes: function (app) {
    app.get('/projects', module.exports.all);
    app.get('/project/:code', module.exports.get);
    app.post('/projects', module.exports.create);
    app.put('/project/:code', module.exports.update);
  }
};