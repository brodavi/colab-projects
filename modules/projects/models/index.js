var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProjectSchema = new Schema({
  code: String,
  name: String,
  team: [{
    name: String,
    base: {
      rate: Number,
      colabRate: Number
    },
    tasks: [
      {
        task: String,
        rate: Number,
        colabRate: Number
      }
    ]
  }]
});

module.exports = process.app.connection.model('Project', ProjectSchema);